from time import sleep

import argparse
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def main():
    parser = initialize_argument_parser()
    args = parser.parse_args()
    driver = webdriver.Firefox()
    login(driver, args)
    select_add_appointment_and_next(driver)
    choose_vaccination_center(driver)
    while("Bitte versuchen Sie es in ein paar Tagen erneut." in driver.page_source):
        driver.find_element_by_id("WorkflowButton-4255").click()
        sleep(3)
        driver.find_element_by_id("WorkflowButton-4212").click()
        sleep(3)


def login(driver, args):
    driver.get("https://sachsen.impfterminvergabe.de/civ.public/start.html?oe=00.00.IM&mode=cc&cc_key=IOAktion")
    wait_until_text_shown(driver, "EditorGroupHeader-9453", "Zugangsdaten")

    vorgangskennung = driver.find_element_by_id("gwt-uid-3")
    vorgangskennung.send_keys(args.vorgangskennung)

    password = driver.find_element_by_id("gwt-uid-5")
    password.send_keys(args.passwort)

    driver.find_element_by_id("WorkflowButton-4212").click()
    sleep(3)


def select_add_appointment_and_next(driver):
    driver.find_element_by_xpath("//label[@for='gwt-uid-9']").click()
    driver.find_element_by_id("WorkflowButton-4212").click()
    sleep(3)


def choose_vaccination_center(driver):
    driver.find_element_by_class_name("select2-selection__arrow").click()
    search_field = driver.find_element_by_class_name("select2-search__field")
    search_field.send_keys("Leipzig", Keys.RETURN)
    driver.find_element_by_id("WorkflowButton-4212").click()
    sleep(3)


def initialize_argument_parser():
    parser = argparse.ArgumentParser(
        description="""
Usage: python convid_vaccination_appointment.py <Vorgangskennung> <Passwort>

Command line tool for makeing an appointment for a Covid vaccination in Leipzig
"""
    )
    parser.add_argument("vorgangskennung", type=str)
    parser.add_argument("passwort")
    parser.add_argument("--browser")
    return parser


def wait_until_text_shown(driver, element_id, searched_text):
    try:
        element_present = EC.text_to_be_present_in_element((By.ID, element_id), searched_text)
        WebDriverWait(driver, timeout=3000).until(element_present)
        print("\n<<Page is ready>>")
    except TimeoutException:
        print("\n<<Loading took too much time!>>")


if __name__ == '__main__':
    main()
